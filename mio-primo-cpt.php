<?php
/**
 * Il mio primo CPT - un plugin per mostrare come realizzare un Custom Post Type in WordPress
 *
 * @package      WordPress
 * @author       SkillsAndMore
 * @license      GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name:       Il mio primo CPT
 * Plugin URI:        https://skillsandmore.org
 * Description:       Con questo plugin creo il mio primo Custom Post Type.
 * Version:           0.0.1
 * Author:            Andrea Barghigiani
 * Author URI:        https://skillsandmore.org
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

add_action( 'init', 'sam_register_first_cpt' );
/**
 * La funzione sam_register_first_cpt() registra all'interno di WordPress il Custom Post Type 'sam_portfolio' agganciandosi all'hook init.
 */
function sam_register_first_cpt() {

	// Creo un array separato per le label per organizzare meglio il mio codice.
	// https://developer.wordpress.org/reference/functions/get_post_type_labels/ Pagina di documentazione per tutte le label che puoi utilizzare.
	$labels = [
		'name' => __( 'Portfolio', 'sam-text' ),
		'singular_name' => __( 'Lavoro', 'sam-text' ),
		'add_new' => __( 'Aggiungi lavoro', 'sam-text' ),
		'add_new_item' => __( 'Aggiungi lavoro', 'sam-text' ),
		'edit_item' => __( 'Modifica lavoro', 'sam-text' ),
		'view_item' => __( 'Visualizza lavoro', 'sam-text' ),
		'not_found' => __( 'Nessun lavoro trovato', 'sam-text' ),
	];

	// Definisco l'array di opzioni per organizzare meglio il mio codice.
	// https://developer.wordpress.org/reference/functions/register_post_type/ Pagina di documentazione per tutte le opzioni che puoi impostare.
	$args = [
		'labels' => $labels, // Inserisco le label dall'array $labels dichiarate in precedenza.
		'public' => true,
		'supports' => [ 'title', 'editor', 'excerpt', 'thumbnail' ],
		'taxonomies' => [ 'category' ],
		'show_in_rest' => true,
	];

	register_post_type( 'sam_portfolio', $args );
}

add_action( 'genesis_after_loop', 'sam_portfolio_loop' );
/**
 * Questa funzione si aggancia all'hook genesis_after_loop disponibile solo per i child theme Genesis.
 * Se vuoi ottenere una tua copia del framework puoi farlo a questo link https://skillsandmore.org/genesis/
 *
 * Puoi utilizzare anche il nostro starter theme dedicato a Genesis che trovi a questo link: https://github.com/skillsAndMore/sam-genesis-dev-starter
 *
 * La funzione sam_portfolio_loop() genera un loop personalizzato con WP_Query che elenca i contenuti del Custom Post Type creato con la funzione sam_register_first_cpt().
 */
function sam_portfolio_loop() {
	$args = [
		'post_type' => 'sam_portfolio',
	];

	$loop = new WP_Query( $args );
	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) {
			$loop->the_post();

			the_title( '<h2>', '</h2>' );
		}
	}
}
